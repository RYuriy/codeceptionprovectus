<?php 

class FirstBrowserCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function testRobotMeta(AcceptanceTester $I)
    {
        $I->amOnUrl("https://people-background-check.com/");
//        $I->canSeeElement('meta', ['name' => 'robots', 'content' => 'index, follow, archive']);
        $meta_robots_content = $I->grabAttributeFrom('meta[name="robots"]', 'content');
        $I->assertEquals('index, follow, archive', $meta_robots_content);

        $I->amOnUrl("https://backgroundcheck.run/");
//        $I->canSeeElement('meta', ['name' => 'robots', 'content' => 'index, follow, archive']);
        $meta_robots_content = $I->grabAttributeFrom('meta[name="robots"]', 'content');
        $I->assertEquals('index, follow, archive', $meta_robots_content);

        $I->amOnUrl("https://veripages.com/");
//        $I->canSeeElement('meta', ['name' => 'robots', 'content' => 'index, follow, archive']);
        $meta_robots_content = $I->grabAttributeFrom('meta[name="robots"]', 'content');
        $I->assertEquals('index, follow, archive', $meta_robots_content);
    }
}
