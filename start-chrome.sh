#!/usr/bin/env bash

SELENIUM_TARGET=driver/selenium-server-standalone.jar
CHROME_DRIVER_TARGET=driver/chromedriver

# Run Chrome via Selenium Server
start() {
    java -Dwebdriver.chrome.driver=${CHROME_DRIVER_TARGET} -jar ${SELENIUM_TARGET}
}

start-without-screen() {
    # ps aux | grep xvfb
    #pkill Xvfb
    xvfb-run java -Dwebdriver.chrome.driver=${CHROME_DRIVER_TARGET} -jar ${SELENIUM_TARGET}
}

start-chrome-debug() {
    xvfb-run java -Dwebdriver.chrome.driver=${CHROME_DRIVER_TARGET} -jar ${SELENIUM_TARGET} -debug
}

# Run Chrome Headless
start-chrome-headless() {
    chromedriver --url-base=/wd/hub
}

 start
# start-without-screen
# start-chrome-debug
# start-chrome-headless
