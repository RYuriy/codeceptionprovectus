set SELENIUM_TARGET="C:\__OPENSERVER__\domains\Codeception\driver\selenium-server-standalone.jar"
set CHROME_DRIVER_TARGET="C:\__OPENSERVER__\domains\Codeception\driver\chromedriver.exe"

# Run Chrome via Selenium Server
java -Dwebdriver.chrome.driver=%CHROME_DRIVER_TARGET% -jar %SELENIUM_TARGET%
