# Usage

### Clone reposytory
```
$ git clone https://RYuriy@bitbucket.org/RYuriy/codeceptionprovectus.git
```

### Go into the created folder
```
$ cd codeceptionprovectus/
```

### Install all dependencies
```
$ composer install
```

### To create new test
```
$ vendor/bin/codecept g:test unit ExampleU
$ vendor/bin/codecept g:cest functional ExampleF
$ vendor/bin/codecept g:cest acceptance ExampleA 
$ vendor/bin/codecept g:cest browser ExampleB 

(you can use g:cept instead g:cest)
```

### Start all tests
```
$ vendor/bin/codecept run 
```
### Start unit tests (the same for other suites: functional, acceptance, browser)
```
$ vendor/bin/codecept run unit
```

### Start tests in browser
```
- Fix paths to selenium server and chromedriver in start-chrome.sh file
- Make files as executable (sudo chmod +x /path_to_file/start-chrome.sh /path_to_file/selenium-server-standalone.jar /path_to_file/chromedriver) 
- start selenium server with chromedriver
$ ./start-chrome.sh
(start-chrome.bat for Windows)

- execute tests
$ vendor/bin/codecept run browser
```

### For non-javascript browser use PhpBrowser and acceptance suite
```
$ vendor/bin/codecept run acceptance

see more info
https://codeception.com/docs/modules/PhpBrowser

```

### See more information
```
https://codeception.com/
```
